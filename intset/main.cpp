#include <iostream>
#include <string>
#include "intset.h"

int main()
{

  IntSet s1 = emptyIS();
  addIS(1, s1);
  addIS(2, s1);
  addIS(3, s1);

  IntSet s2 = emptyIS();
  addIS(3, s2);
  addIS(5, s2);
  addIS(2, s2);
  addIS(6, s2);

  IntSet s3 = unionIS(s1, s2);

  //printIS(s3);

  IntSet s4 = insersectIS(s1,s2);

  //printIS( s4);


  std::cout<< "primer elemento del array: " << (toArrayIS(s4))[1] << std::endl;
  destroyIS(s3);
  //IntSet interse = insersectIS(s1,s2);

  //std::cout << "interse: "; printIS(interse);
  // removeIS(4, s1);
  // std::cout << "removeS(4,s1) = ";
  // printIS(s1);
  // removeIS(3, s1);
  // std::cout << "removeS(3,s1) = ";
  // printIS(s1);

  //cout << "setToList(s1) = ";
  //printList(setToList(s1));

  return 0;
}
