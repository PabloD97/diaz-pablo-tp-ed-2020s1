#include <iostream>
#include <string>
#include "intset.h"

struct IntSetNode {
    int elem;
    IntSetNode* next;
};

// Construye un conjunto de enteros vacio
IntSet emptyIS(){
    return NULL;
}

// Permite determinar si un entero pertenece al conjunto
bool belongsIS(int x, IntSet s){

    bool res=false;
    IntSet sett = s;
    while( res != true && sett != NULL){
        if( sett->elem == x ){
            res = true;
        }
        sett = sett->next;
    }
    return res;
}


// Agrega un entero al conjunto
void addIS(int x, IntSet& s){
    if(!belongsIS(x,s)){ // o(n)
        IntSetNode* node = new IntSetNode;
        node->elem = x;
        node->next = s;
        s = node;
    }
}


// Remueve un entero del conjunto
void removeIS(int x, IntSet& s){
    if( x == s->elem ){
        s = s->next;
    }
    else{
        IntSetNode* setARecorrer = s;
        IntSetNode* resultado = emptyIS();

        while(setARecorrer->elem != x){

                addIS(setARecorrer->elem, resultado);
                setARecorrer = setARecorrer->next;
        }
        std::cout<< "este es el elemento que va ser removido del set: " << setARecorrer->elem << std::endl;
        if( setARecorrer->next != NULL ){
            addIS(setARecorrer->next->elem,resultado);
            if( setARecorrer->next->next != NULL ){
                IntSetNode* nodo = new IntSetNode;
                nodo->elem = setARecorrer->next->next->elem;
                nodo->next = setARecorrer->next->next->next;
                resultado->next=nodo;
            }
        }
        s = resultado;
    }
}

// Permite determinar la cantidad de elementos
int sizeIS(IntSet s){
    int largo = 0;
    IntSetNode* setito = s;
    while( setito != NULL ){
        setito = setito->next;
        largo++;
    }
    return largo;
}

// Union de los dos conjuntos
IntSet unionIS(IntSet s1, IntSet s2){
    IntSetNode* setARecorrer = s2;

    while(setARecorrer->next != NULL){
        addIS(setARecorrer->elem,s1);
        setARecorrer = setARecorrer->next;
    }
    addIS(setARecorrer->elem, s1);
    return s1;
}


// Interseccion de los dos conjuntos
IntSet insersectIS(IntSet s1, IntSet s2){
    IntSetNode* res = emptyIS();
    IntSetNode* setARecorrer = s2;

    while(setARecorrer->next != NULL){
        if(belongsIS(setARecorrer->elem, s1 )){
            addIS(setARecorrer->elem, res);
        }
        setARecorrer = setARecorrer->next;
    }

    if(belongsIS(setARecorrer->elem,s1)){
        addIS(setARecorrer->elem, res);
    }

    return res;
}



// Permite obtener un array con los enteros del conjunto
int* toArrayIS(IntSet s){
    int length = sizeIS(s);
    IntSetNode* copia = s;
    int* toArray = new int[length];
    for(int i = 0; i < length; i++){
        toArray[i] = copia->elem;
        copia = copia->next;
    }
    return toArray;
}

// Elimina el conjunto
void destroyIS(IntSet& s){

    IntSetNode* recorrer = s->next;

    while(recorrer != NULL){
        IntSetNode* siguiente = recorrer->next;
        delete recorrer;
        recorrer = siguiente;
    }

    delete s;
    std::cout << "El set fue borrado" <<std::endl;
}

// Emprime los valores del conjunto en standard output
void printIS(IntSet s) {
    if(s != NULL){
        IntSetNode* s1 = s;
        int length = sizeIS(s);
        std::cout << "Direccion de memoria del set: " << s << std::endl;
        std::cout << "Los elementos del set son: " << std::endl;
        for(int i = 0; i< length ; i++){
            std::cout << "  elemento " << i << ": " << s1->elem << std::endl;
            removeIS(s1->elem,s1);
        }
    }
}

