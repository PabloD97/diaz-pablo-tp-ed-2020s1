#include <iostream>
#include <string>
#include "pokemon.h"

using namespace std;

struct PokemonSt{
    std::string nombre;
    TipoDePokemon tipoDePokemon;
    int porcentajeDeEnergia;
};

Pokemon mkPokemon(std::string nombre, TipoDePokemon tipoDePokemon){
    Pokemon dirP = new PokemonSt;
    dirP->nombre    = nombre;
    dirP->tipoDePokemon      = tipoDePokemon;
    dirP->porcentajeDeEnergia = 100;

    return dirP;
}

// indica si el pokemon es de dicho tipo
bool esTipoDePokemon(Pokemon pokemon, TipoDePokemon tipoDePokemon){
    return pokemon->tipoDePokemon == tipoDePokemon;
}

bool superaA(Pokemon primero, Pokemon segundo){
   if(primero->tipoDePokemon == AGUA && segundo->tipoDePokemon == FUEGO){
        return true;
   }
   if(primero->tipoDePokemon == FUEGO && segundo->tipoDePokemon == PLANTA){
        return true;
   }
   if(primero->tipoDePokemon == PLANTA && segundo->tipoDePokemon == AGUA){
        return true;
   }
   return false;
}



void destroyPokemon(Pokemon pokemon){
    delete pokemon;
}
