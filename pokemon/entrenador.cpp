#include <iostream>
#include <string>
#include "entrenador.h"
#include "pokemon.h"


using namespace std;

struct EntrenadorSt {
    string nombre;
    Pokemon* pokemones;
    int cantPokemones = 0;
};

// contruye un entrenador
Entrenador mkEntrenador(std::string nombre){

    Entrenador dirE = new EntrenadorSt;
    dirE->nombre    = nombre;

    return dirE;
}

// Agrega dicho pokemon a la lista del entrenador
void capturar(Entrenador entrenador, Pokemon pokemon){
    Pokemon* pokemonesN = new Pokemon[entrenador->cantPokemones + 1];
    Pokemon* anterior  = entrenador->pokemones;

    for(int index=0; index < entrenador->cantPokemones; index++){
        pokemonesN[index] = anterior[index];
    }

    pokemonesN[entrenador->cantPokemones] = pokemon;

    entrenador->pokemones = pokemonesN ;
    entrenador->cantPokemones = entrenador->cantPokemones + 1;
}

// Devuelve la cantidad de pokemon que posee el entrenador.
int cantPokemones(Entrenador entrenador){
    return entrenador->cantPokemones;
}

// Devuelve la cantidad de pokemon de determinado tipo que posee el entrenador.
int cantPokemonesDe(Entrenador entrenador, TipoDePokemon tipoDePokemon){

    int res = 0;
    Pokemon* pokemones = entrenador->pokemones;

    for(int index = 0; index < entrenador->cantPokemones; index++){
        if ( esTipoDePokemon(pokemones[index], tipoDePokemon)){
            res++;
        }
    }

    return res;
}

// Dada una array de entrenadores devuelve la concatenacion de todos sus pokemon.
Pokemon* concatPokemon(Entrenador* entrenadores, int cantidadEntrenadores){

    Pokemon* poke = new Pokemon[ cantDePokemonesDeEntrenadores(entrenadores,cantidadEntrenadores) ];

    for(int index = 0 ; index < cantidadEntrenadores; index++){
        Pokemon* anteriores = entrenadores[index]->pokemones;
        for(int index2   = 0; index2 < entrenadores[index]->cantPokemones ; index2++ ){
            poke[index2] = anteriores[index2];
            // se que es mal el segundo for, por que pisa los pokemones del anterior index
        }
    }
    return poke;
}

int cantDePokemonesDeEntrenadores(Entrenador* entrenadores, int cantidadEntrenadores){
    int res = 0;
    for(int index = 0; index < cantidadEntrenadores ; index++){
        res = res + cantPokemones(entrenadores[index]);
    }
    return res;
}

// Dado un entrenador, devuelve True si posee al menos un pokemon de cada tipo posible.
bool esMaestro(Entrenador entrenador){

    int resMayor3 = 0;
    const char* tipos[] = {FUEGO,AGUA,PLANTA};
    // Trate de inicializar el array con TipoDePokemon pero no me lo permitio.
    // Me pedia que lo inicialice con const char*
    for( int index = 0 ; index < 3 ; index++){
        if ( cantPokemonesDe(entrenador, tipos[index]) > 0 ){
            resMayor3++;
        }
    }
    return resMayor3 >= 3;
}

// Dado un entrenador libera la memoria ocupada por el mismo
void destroyEntrenador(Entrenador entrenador){
    delete entrenador;
}
