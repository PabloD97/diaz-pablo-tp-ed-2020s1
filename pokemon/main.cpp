#include <iostream>
#include "entrenador.h"
#include <string>

using namespace std;

int main()
{
    Entrenador ash = mkEntrenador("ash");
    Entrenador brok = mkEntrenador("brok");

    Pokemon pikachu = mkPokemon("pikachu", AGUA);
    Pokemon agumon = mkPokemon("agumon", FUEGO);
    Pokemon agumon2 = mkPokemon("agumon2", FUEGO);


    capturar(ash, pikachu);
    capturar(ash, agumon);
    capturar(ash, agumon2);

    capturar(brok, agumon2);
    capturar(brok, agumon);

    Entrenador* entrenadores = new Entrenador[2];
    entrenadores[0] = ash;
    entrenadores[1] = brok;

    int dos = 2;

    std::cout << "Codigo del entrenador ash: " << ash << std::endl;

    std::cout << "cuantos pokemones posee " << cantPokemones(ash) << std::endl;

    std::cout << "cuantos pokemones de tipo agua hay? "<<cantPokemonesDe(ash, AGUA) << std::endl;

    std::cout << "es maestro? Si es 1 la respuesta es si, si es 0 la respuesta es no: " << esMaestro(ash) << std::endl;

    std::cout << "entre todos los entrenadores tienen en total " << cantDePokemonesDeEntrenadores(entrenadores, 2) << " pokemones" << std::endl;


    //destroyEntrenador(ash);
    //std::cout <<  entrenadores << std::endl;

    return 0;
}

